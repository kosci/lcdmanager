#!/usr/bin/python
# -*- coding: utf-8 -*-
#pylint: skip-file

"""Tests for canvas class"""
from builtins import range
from builtins import object

__author__ = 'Bartosz Kościów'

from nose.tools import assert_equal
from nose.tools import assert_raises
import lcdmanager.widget.canvas as canvas


class TestCanvas(object):
    def setUp(self):
        self.canvas = canvas.Canvas(1, 1, 11, 2, 'New', True)
        self.buffer = [" ".ljust(11, " ") for i in range(0, 2)]

    def test_should_initialize_correctly(self):
        assert_equal(self.canvas.position, {'x': 1, 'y': 1})
        assert_equal(self.canvas.x, 0)
        assert_equal(self.canvas.y, 0)
        assert_equal(self.canvas.visibility, True)
        assert_equal(self.canvas.autowidth, False)
        assert_equal(self.canvas.width, 11)
        assert_equal(self.canvas.height, 2)

    def test_set_cursor_position(self):
        self.canvas.x = 3
        self.canvas.y = 1
        assert_equal(self.canvas.x, 3)
        assert_equal(self.canvas.y, 1)

    def test_write_string(self):
        self.canvas.write("It's me")
        assert_equal(self.canvas.x, 7)
        self.buffer[0] = "It's me    "
        assert_equal(self.canvas.render(), self.buffer)

    def test_write_string_at_position(self):
        self.canvas.write("It's me", 3, 1)
        assert_equal(self.canvas.x, 10)
        self.buffer[1] = "   It's me "
        assert_equal(self.canvas.render(), self.buffer)

    def test_write_string_at_position_with_line_break(self):
        self.canvas.write("It's me", 7, 0)
        assert_equal(self.canvas.x, 11)
        self.buffer[0] = "       It's"
        assert_equal(self.canvas.render(), self.buffer)

    def test_write_string_at_position_out_of_bonds(self):
        self.canvas.write("It's me", 13, 11)
        assert_equal(self.canvas.x, 11)
        assert_equal(self.canvas.y, 2)
        assert_equal(self.canvas.render(), self.buffer)

    def test_write_three_strings(self):
        self.canvas.write("It's", 1)
        self.canvas.write(" me")
        self.canvas.write("For", 9, 0)
        self.canvas.write("sure", 2, 1)
        self.buffer[0] = " It's me Fo"
        self.buffer[1] = "  sure     "
        assert_equal(self.canvas.render(), self.buffer)

    def test_clear(self):
        self.canvas.write("It's", 1)
        self.canvas.write(" me")
        self.canvas.clear()
        assert_equal(self.canvas.render(), self.buffer)
