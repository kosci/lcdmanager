#!/usr/bin/python
# -*- coding: utf-8 -*-
#pylint: skip-file

"""Tests for widget abstract class"""
from builtins import object

__author__ = 'Bartosz Kościów'

from nose.tools import assert_equal
from nose.tools import assert_raises
import lcdmanager.abstract.widget as widget


class TestWidget(object):
    def setUp(self):
        self.widget = widget.Widget(12, 1, 'New', True)

    def test_should_initialize_correctly(self):
        assert_equal(self.widget.position, {'x': 12, 'y': 1})
        assert_equal(self.widget.autowidth, True)
        assert_equal(self.widget.width, 0)
        assert_equal(self.widget.autoheight, True)
        assert_equal(self.widget.height, 0)

    def test_should_set_position_0_0(self):
        self.widget.position = {'x': 0, 'y': 0}

    def test_should_set_position_(self):
        self.widget.position = {'x': 3, 'y': 4}
        assert_equal(self.widget.position, {'x': 3, 'y': 4})
        assert_raises(AttributeError, setattr, self.widget, 'position', 'abc')

    def test_should_change_visibility(self):
        assert_equal(self.widget.visibility, True)
        self.widget.visibility = False
        assert_equal(self.widget.visibility, False)
        assert_raises(AttributeError, setattr, self.widget, 'visibility', 'abc')

    def test_should_change_name(self):
         assert_equal(self.widget.name, 'New')
         self.widget.name = "Newer"
         assert_equal(self.widget.name, 'Newer')
