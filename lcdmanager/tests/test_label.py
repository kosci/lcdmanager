#!/usr/bin/python
# -*- coding: utf-8 -*-
#pylint: skip-file

"""Tests for label class"""
from builtins import object

__author__ = 'Bartosz Kościów'

from nose.tools import assert_equal
from nose.tools import assert_raises
import lcdmanager.widget.label as label


class TestLabel(object):
    def setUp(self):
        self.label = label.Label(12, 1, 'New', True)

    def test_should_initialize_correctly(self):
        assert_equal(self.label.position, {'x': 12, 'y': 1})
        assert_equal(self.label.visibility, True)
        assert_equal(self.label.autowidth, True)
        assert_equal(self.label.width, 0)

    def test_should_have_label(self):
        self.label.label = "It's label"
        assert_equal(self.label.label, ["It's label"])

    def test_should_render(self):
        self.label.label = "It's label"
        assert_equal(self.label.render(), ["It's label"])

    def test_should_render_empty_view(self):
        assert_equal(self.label.render(), [""])

    def test_setting_label_should_change_widget_width(self):
        self.label.label = "It's label"
        assert_equal(self.label.width, 10)

    def test_setting_width_should_disable_autowidth(self):
        self.label.width = 14
        assert_equal(self.label.autowidth, False)

    def test_setting_enough_width_should_not_crop_text(self):
        self.label.width = 14
        self.label.label = "It's a label"
        assert_equal(self.label.render(), ["It's a label"])

    def test_setting_small_width_should_crop_text(self):
        self.label.width = 4
        self.label.label = "It's a label"
        assert_equal(self.label.render(), ["It's"])

    def test_change_width_should_update_display(self):
        self.label.width = 4
        self.label.label = "It's a label"
        self.label.width = 12
        assert_equal(self.label.render(), ["It's a label"])

    def test_change_to_autowidth_should_change_width(self):
        self.label.label = "It's a label"
        self.label.width = 4
        self.label.autowidth = True
        assert_equal(self.label.render(), ["It's a label"])
        assert_equal(self.label.width, 12)

    def test_should_assign_multiline_text(self):
        self.label.label = ['line one', 'second line']
        assert_equal(self.label.render(), ['line one', 'second line'])
        assert_equal(self.label.height, 2)

    def test_should_change_height(self):
        self.label.label = ['line one', 'second line', '3rd line']
        self.label.height = 2
        assert_equal(self.label.render(), ['line one', 'second line'])
        assert_equal(self.label.height, 2)

    def test_setting_autoheight_should_change_height(self):
        self.label.label = ['line one', 'second line', '3rd line']
        self.label.height = 2
        self.label.autoheight = True
        assert_equal(self.label.render(), ['line one', 'second line', '3rd line'])
        assert_equal(self.label.height, 3)

    def test_change_label_should_change_width_when_autowidth(self):
        self.label.label = "It's a label"
        assert_equal(self.label.width, 12)
        self.label.label = "The label"
        assert_equal(self.label.autowidth, True)
        assert_equal(self.label.render(), ['The label'])
        assert_equal(self.label.width, 9)
