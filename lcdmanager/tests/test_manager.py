#!/usr/bin/python
# -*- coding: utf-8 -*-
#pylint: skip-file

"""Tests for manager"""
from builtins import range
from builtins import object
from nose.tools import assert_equal
from nose.tools import assert_raises
from mock import MagicMock
import charlcd.buffered as buffered
import charlcd.direct as direct
from charlcd.drivers.null import Null
from lcdmanager import manager
import lcdmanager.widget.label as label
import lcdmanager.widget.button as button


class TestManager(object):
    def setUp(self):
        self.lcd = buffered.CharLCD(20, 4, Null())
        self.lcd.init()
        self.lcd_manager = manager.Manager(self.lcd)
        self.lcd_buffer = [" ".ljust(20, " ") for i in range(0, 4)]

    def test_is_initialized_correctly(self):
        lcd = buffered.CharLCD(16, 2, Null())
        lcd_manager = manager.Manager(lcd)
        assert_equal(lcd_manager.widgets, [])
        assert_equal(lcd_manager.name_widget, {})

    def test_should_fail_initialization(self):
        lcd = direct.CharLCD(16,2, Null())
        assert_raises(AttributeError, manager.Manager, lcd)

    def test_should_return_width(self):
        assert_equal(self.lcd_manager.width, 20)

    def test_should_add_label_without_name_to_widgets_dict(self):
        widget_label = label.Label(1, 1)
        widget_label.label = "name"
        self.lcd_manager.add_widget(widget_label)
        assert_equal(self.lcd_manager.widgets, [widget_label])

    def test_should_add_label_with_name_to_widgets_dict(self):
        widget_label = label.Label(1, 1, 'text1')
        widget_label.label = "name"
        self.lcd_manager.add_widget(widget_label)
        assert_equal(self.lcd_manager.widgets, [widget_label])

    def test_should_return_widget_by_name(self):
        widget_label = label.Label(1, 1, 'text1')
        widget_label.label = "name"
        self.lcd_manager.add_widget(widget_label)
        assert_equal(self.lcd_manager.get_widget('text1'), widget_label)

    def test_should_not_return_widget_by_name(self):
        widget_label = label.Label(1, 1, 'text1')
        widget_label.label = "name"
        self.lcd_manager.add_widget(widget_label)
        assert_equal(self.lcd_manager.get_widget('text2'), None)

    def test_should_render_one_label(self):
        widget_label = label.Label(1, 1)
        widget_label.label = "name"
        self.lcd_manager.add_widget(widget_label)
        self.lcd_manager.render()
        buffer = self.lcd_buffer
        buffer[1] = " name               "
        assert_equal(self.lcd_manager.lcd.buffer, buffer)

    def test_should_call_lcd_flush(self):
        self.lcd_manager.lcd.flush = MagicMock()
        self.lcd_manager.flush()
        self.lcd_manager.lcd.flush.assert_called_once_with()

    def test_should_render_multiline_label(self):
        widget_label = label.Label(1, 1)
        widget_label.label = ["name", "surname"]
        self.lcd_manager.add_widget(widget_label)
        self.lcd_manager.render()
        buffer = self.lcd_buffer
        buffer[1] = " name               "
        buffer[2] = " surname            "
        assert_equal(self.lcd_manager.lcd.buffer, buffer)

    def test_two_widgets_should_render_next_to_each_other(self):
        widget_label1 = label.Label(1, 1)
        widget_label1.label = "Hi "
        widget_label2 = label.Label(4, 1)
        widget_label2.label = "Ho "
        self.lcd_manager.add_widget(widget_label1)
        self.lcd_manager.add_widget(widget_label2)
        self.lcd_manager.render()
        buffer = self.lcd_buffer
        buffer[1] = " Hi Ho              "
        assert_equal(self.lcd_manager.lcd.buffer, buffer)
