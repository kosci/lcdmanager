# -*- coding: utf-8 -*-
#pylint: skip-file

"""Tests for manager"""
from builtins import range
from builtins import object
from nose.tools import assert_equal
from nose.tools import assert_raises
from mock import MagicMock
import charlcd.buffered as buffered
import charlcd.direct as direct
from charlcd.drivers.null import Null
from lcdmanager import manager
import lcdmanager.widget.label as label
import lcdmanager.widget.button as button

from lcdmanager import display

class TestDisplay(object):
    def setUp(self):
        self.display = display.Display()
        self.lcd = buffered.CharLCD(20, 4, Null())
        self.lcd_manager = manager.Manager(self.lcd)

    def test_is_initialized_correctly(self):
        assert_equal(self.display.fps, 1)
        assert_equal(self.display.managers, {})

    def test_should_add_manager(self):
        self.display.add(self.lcd_manager, 'one')
        assert_equal(self.display.managers, {'one': self.lcd_manager})

    def test_should_return_mnagers_name(self):
        self.display.add(self.lcd_manager, 'one')
        self.display.add(self.lcd_manager, 'two')
        assert_equal(sorted(self.display.names), ['one', 'two'])
