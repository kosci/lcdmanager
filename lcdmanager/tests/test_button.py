#!/usr/bin/python
# -*- coding: utf-8 -*-
#pylint: skip-file

"""Tests for label class"""
from builtins import object

__author__ = 'Bartosz Kościów'

from nose.tools import assert_equal
from nose.tools import assert_raises
import lcdmanager.widget.button as button
import lcdmanager.abstract.event.focus as focus
import lcdmanager.abstract.event.action as action


class TestButton(object):
    def setUp(self):
        self.button = button.Button(12, 1, 'New', True)

    def test_should_initialize_correctly(self):
        assert_equal(self.button.position, {'x': 12, 'y': 1})
        assert_equal(self.button.visibility, True)
        assert_equal(self.button.autowidth, True)
        assert_equal(self.button.width, 0)
        assert_equal(self.button.has_focus, False)

    def test_should_implement_event_focus_interface(self):
        assert_equal(isinstance(self.button, focus.Focus), True)

    def test_should_implement_event_action_interface(self):
        assert_equal(isinstance(self.button, action.Action), True)

    def test_should_have_label(self):
        self.button.label = "It's label"
        assert_equal(self.button.label, ["It's label"])

    def test_should_render(self):
        self.button.label = "It's label"
        assert_equal(self.button.render(), ["It's label"])

    def test_should_render_empty_view(self):
        assert_equal(self.button.render(), [""])

    def test_setting_label_should_change_widget_width(self):
        self.button.label = "It's label"
        assert_equal(self.button.width, 10)

    def test_setting_width_should_disable_autowidth(self):
        self.button.width = 14
        assert_equal(self.button.autowidth, False)

    def test_setting_enough_width_should_not_crop_text(self):
        self.button.width = 14
        self.button.label = "It's a label"
        assert_equal(self.button.render(), ["It's a label"])

    def test_setting_small_width_should_crop_text(self):
        self.button.width = 4
        self.button.label = "It's a label"
        assert_equal(self.button.render(), ["It's"])

    def test_change_width_should_update_display(self):
        self.button.width = 4
        self.button.label = "It's a label"
        self.button.width = 12
        assert_equal(self.button.render(), ["It's a label"])

    def test_change_to_autowidth_should_change_width(self):
        self.button.label = "It's a label"
        self.button.width = 4
        self.button.autowidth = True
        assert_equal(self.button.render(), ["It's a label"])
        assert_equal(self.button.width, 12)

    def test_should_assign_multiline_text(self):
        self.button.label = ['line one', 'second line']
        assert_equal(self.button.render(), ['line one', 'second line'])
        assert_equal(self.button.height, 2)

    def test_should_change_height(self):
        self.button.label = ['line one', 'second line', '3rd line']
        self.button.height = 2
        assert_equal(self.button.render(), ['line one', 'second line'])
        assert_equal(self.button.height, 2)

    def test_setting_autoheight_should_change_height(self):
        self.button.label = ['line one', 'second line', '3rd line']
        self.button.height = 2
        self.button.autoheight = True
        assert_equal(self.button.render(), ['line one', 'second line', '3rd line'])
        assert_equal(self.button.height, 3)

    def test_focus_should_change_variable(self):
        self.button.event_focus()
        assert_equal(self.button.has_focus, True)

    def test_pointer_setters_getters_should_work(self):
        self.button.pointer_before = "+"
        self.button.pointer_after = "-"
        assert_equal(self.button.pointer_before, "+")
        assert_equal(self.button.pointer_after, "-")

    def test_render_focused_default_options(self):
        self.button.label = " Start game "
        self.button.event_focus()
        assert_equal(self.button.render(), ['>Start game<'])

    def test_render_focused_left_marker_options(self):
        self.button.pointer_after = ''
        self.button.label = " Start game"
        self.button.event_focus()
        assert_equal(self.button.render(), ['>Start game'])

    def test_render_focused_right_marker_options(self):
        self.button.pointer_before = ''
        self.button.label = "Start game "
        self.button.event_focus()
        assert_equal(self.button.render(), ['Start game<'])

    def test_should_attach_and_call_callback(self):
        self.button.label = " Start game"

        def _log(widget):
            return widget.label

        self.button.callback = _log
        assert_equal(self.button.callback, _log)
        assert_equal(self.button.event_action(), self.button.label)
