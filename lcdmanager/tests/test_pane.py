#!/usr/bin/python
# -*- coding: utf-8 -*-
#pylint: skip-file

"""Tests for pane widget"""
from builtins import object

__author__ = 'Bartosz Kościów'

from nose.tools import assert_equal
from nose.tools import assert_raises
import lcdmanager.widget.pane as pane
import lcdmanager.widget.label as label


class TestPane(object):
    def setUp(self):
        self.pane = pane.Pane(1, 1, 'Tab1')

    def test_is_initialized_correctly(self):
        assert_equal(self.pane.visibility, True)
        assert_equal(self.pane.position, {'x': 1, 'y': 1})
        assert_equal(self.pane.autowidth, True)
        assert_equal(self.pane.width, 0)

    def test_can_hold_widgets(self):
        widget_label = label.Label(1, 1)
        widget_label.label = "name"
        self.pane.add_widget(widget_label)
        assert_equal(self.pane.widgets, [widget_label])
        widget_label_1 = label.Label(0, 2)
        widget_label_1.label = "eman"
        self.pane.add_widget(widget_label_1)
        assert_equal(self.pane.widgets, [widget_label, widget_label_1])

    def test_should_return_widget_by_name(self):
        widget_label = label.Label(1, 1, 'text1')
        widget_label.label = "name"
        self.pane.add_widget(widget_label)
        assert_equal(self.pane.get_widget('text1'), widget_label)

    def test_should_render_label_from_top(self):
        widget_label = label.Label(1, 0)
        widget_label.label = "name"
        self.pane.add_widget(widget_label)
        output = [
            " name"
        ]
        assert_equal(self.pane.render(), output)

    def test_should_render_two_labels(self):
        widget_label = label.Label(1, 0)
        widget_label_1 = label.Label(0, 2)
        widget_label.label = "name"
        widget_label_1.label = "eman"
        self.pane.add_widget(widget_label)
        self.pane.add_widget(widget_label_1)
        output = [
            " name",
            "     ",
            "eman "
        ]
        assert_equal(self.pane.render(), output)

    def test_get_correct_width_when_autowidth_is_on(self):
        widget_label = label.Label(1, 0)
        widget_label_1 = label.Label(0, 2)
        widget_label.label = "names"
        widget_label_1.label = "eman"
        self.pane.add_widget(widget_label)
        self.pane.add_widget(widget_label_1)
        assert_equal(self.pane.width, 6)

    def test_get_correct_width_when_autowidth_is_off(self):
        self.pane.width = 7
        widget_label = label.Label(1, 0)
        widget_label_1 = label.Label(0, 2)
        widget_label.label = "names"
        widget_label_1.label = "eman"
        self.pane.add_widget(widget_label)
        self.pane.add_widget(widget_label_1)
        assert_equal(self.pane.width, 7)

    def test_will_render_overlapping_widgets(self):
        widget_label = label.Label(1, 0)
        widget_label_1 = label.Label(4, 0)
        widget_label.label = "123"
        widget_label_1.label = "456"
        self.pane.add_widget(widget_label)
        self.pane.add_widget(widget_label_1)
        output = [
            " 123456"
        ]
        assert_equal(self.pane.render(), output)

    def test_get_correct_height_when_autoheigh_is_on(self):
        widget_label = label.Label(1, 0)
        widget_label_1 = label.Label(0, 2)
        widget_label.label = "names"
        widget_label_1.label = "eman"
        self.pane.add_widget(widget_label)
        self.pane.add_widget(widget_label_1)
        assert_equal(self.pane.height, 3)

    def test_get_correct_width_when_autowidth_is_off(self):
        self.pane.height = 7
        widget_label = label.Label(1, 0)
        widget_label_1 = label.Label(0, 2)
        widget_label.label = "names"
        widget_label_1.label = "eman"
        self.pane.add_widget(widget_label)
        self.pane.add_widget(widget_label_1)
        assert_equal(self.pane.height, 7)

    def test_two_widgets_should_render_next_to_each_other(self):
        widget_label1 = label.Label(1, 1)
        widget_label1.label = "Hi "
        widget_label2 = label.Label(4, 1)
        widget_label2.label = "Ho "
        self.pane.add_widget(widget_label1)
        self.pane.add_widget(widget_label2)
        output = [
            "       ",
            " Hi Ho "
        ]
        assert_equal(self.pane.render(), output)

    def test_pane_should_render_blank_bottom_line(self):
        widget_label1 = label.Label(8, 1)
        self.pane.height = 4
        self.pane.width = 20
        widget_label1.label = "Some string "
        self.pane.add_widget(widget_label1)
        output = [
            "                    ",
            "        Some string ",
            "                    ",
            "                    ",
        ]
        assert_equal(self.pane.render(), output)
