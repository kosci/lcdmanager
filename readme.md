LCD manager - what it is ?
===

A simple window manager for char lcd based on HD44780. It uses CharLCD package [CharLCD](https://bitbucket.org/kosci/charlcd)
to communicate with lcd. Works with buffered lcd or buffered vlcd. 

Imagine that you want to build simple gui, display some information from sensors or any other info. With this package
you are just adding widgets, its content and position, forget about manually positioning chars. You want some info on LCD and this
package helps you with it.  
It makes creating GUI easier. You don't have to focus how to display something just use widgets.

Currently available widgets:

    - label - display text as one line or multi line
    - pane - conainer for other widgets may be uses as tabs
    - button - simple button
    - canvas - allows to draw like on pure CharLCD

    
See more on: [koscis.wordpress.com](https://koscis.wordpress.com)    

Each widget has its properties, shared are:

    - x and y - position on screen or in pane
    - width and height - it may be automode or manual mode 
    - visibility
    
## Demo

In demos directory you may find some samples.
Good idea is to look at [Piader v2](https://bitbucket.org/kosci/piader) it is simple game based on this package. 
It has three tabs, one for menu one for options and one for game. It shows how to use labels, buttons pane and canvas.


## Initialize LCD Manager and flush content to display

Initialisation require one parameter, an instance of CharLCD class. Manager reads size from it and prepare buffer.  
To display content on screen you have to call two functions. First render - it gather all visible widgets and write chars to buffer. 
And call flush - this writes buffer content to display using CharLCD package and its drivers.
    
    lcd = buffered.CharLCD(20, 4, gp.Gpio())
    lcd.init()
    lcd_manager = manager.Manager(lcd)
    ...
    lcd_manager.render()
    lcd_manager.flush()

Each widget may be created with or without name. Name is useful for getting it from manager without keeping any references 
in your code.
 
    lcd_manager.add_widget(new Label(0, 0, 'name'))
    lcd_manager.get_widget('name').label = 'hi'
     

By default widget tries to adjust automatically width and height. Auto size is disabled when you specify width or height


    label1 = Label(0, 0)
    label1.label = "Hello !"
    label1.width = 7
    label1.height = 1
    
    
or you may disable autosize by

    label.autowidth = False

Display server
===
     
To refresh lcds automatically add them to Display class and start thread. You set fps and auto_render flag. 
If auto_render set to True Display will do both render and flush. With false only flushes. 

Constructor: **Display(fps, auto_render)**


    self.display = display.Display(0.5, True)
    self.display.add(self.game_manager, 'one')
    self.display.start()

Remote server
===

This one starts a server that listen for incoming connections. If someone connect he/she may issue commands:

    list managers - list manager names that are in Display, string separated by ,
    get <name> - get content of names manager , strings separated by \n
    
Remote connection is insecure and it is very bad idea to open server to world.

To start a server use: ```disp.start_remote(1301, '0.0.0.0')```. First parameter is port and second ip address.

    lcd = buffered.CharLCD(20, 4, gp.Gpio())
    lcd.init()
    lcd_manager = manager.Manager(lcd)

    self.display = display.Display(0.5, True)
    self.display.add(self.game_manager, 'one')
    self.display.start()
    self.display.start_remote()

    label1 = Label(0, 0)
    lcd_manager.add_widget(label1)

If you connect to it via telnet you may send commands. 
 
There is an example in demos folder: **remote_read.py**
    
    

Widgets
===

## Label widget

Simple text display in single line or multiline. 

Constructor: **Label(x, y, name, visibility)**

Setting content goes by **label** property. May be string or array
     

- single line


    label1 = Label(0, 0)
    label1.label = "Hello !"
    lcd_manager.add_widget(label1)
    lcd_manager.render()
    lcd_manager.flush()
    label1.label = 'one'
    
- multiline


    label1 = Label(0, 0)
    label1.label = "Hello !"
    lcd_manager.add_widget(label1)
    lcd_manager.render()
    lcd_manager.flush()
    label1.label = ['one', 'two', 'three']
    
## Pane widget 

Container for other widgets. It is easy to simulate tabs. Put few panes on each other and use
visibility to set active one.

**add_widget** - add widget

**get_widget** - retrieve widget by name

**recalculate_width** - recalculate pane width while in auto-width mode.

**recalculate_height** - recalculate pane height while in auto-height mode

bug:
- won't recalculate width & height when inner widget changes. Recallculation must be called manually


    label1 = Label(0, 0)
    label1.label = "1 - Hello !"
    label2 = Label(1, 2)
    label2.label = "3 - Meow !"
    label3 = Label(1, 1)
    label3.label = "2 - Woof !"

    pane1 = Pane(1, 0)
    pane1.add_widget(label1)
    pane1.add_widget(label2)
    pane1.add_widget(label3)
    lcd_manager.add_widget(pane1)

## Button widget

This button react to event. Event server is not included into manager but look at Piader.

Widget has three events:

**focus** - called when we focus on it, it change view by adding markers

**blur** - called while losing focus

**action** - callback 

Functions:

**pointer_before** - property to set or get char that is displayed before label

**pointer_after** - property to set or get char that is displayed before label

**label** - property to set button label. You must leave space for pointers, 
    setting like this ' click ' change during focus state to '>click<' but
  'click' change to '>lic<'

**event_focus** - call it to set this widget as active

**event_blur** - sets as inactive

**event_action** - this one calls callback. To callback reference to widget is passed. 


Before and after overwrite first and last char


    btn1 = Button(0, 0)
    btn1.label = " Start "
    btn1.pointer_before = "*"
    btn1.pointer_after = "*"



calling focus/blur, set active and inactive state    


    btn1.event_focus()
    btn1.event_blur()
    
    
- callback, to callback current widget is passed


    def _log(widget):
        return widget.label
    btn1.callback = _log     
    btn1.event_action()
    
    
## Canvas widget

Simple widget to draw any string on it. 
You must declare its size


    c = new Canvas(0, 0, 13, 2)
    c.write('Hello')
    c.write('Hi !', 0, 1)
    c.clear()


Class diagram
===

    display.Display  -->  Thread

    server.ListenerThread  -->  Thread

    server.ReadThread  -->  Thread

    button.Button  -->  widget.Widget
                   -->  focus.Focus
                   -->  action.Action

    canvas.Canvas  -->  widget.Widget

    label.Label   -->  widget.Widget

    pane.Pane  -->  widget.Widget

    manager.Manager